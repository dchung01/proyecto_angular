import { Component, OnInit} from '@angular/core';
import {DestinosApiClient} from '../../models/destinos-api-client.model';
import {Destinoviaje} from '../../models/destino-viaje.model';
import {ActivatedRoute} from '@angular/router';


// tslint:disable-next-line: class-name
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [DestinosApiClient]
})
export class DestinoDetalleComponent implements OnInit {
 public destino: Destinoviaje;

 style = {
  sources: {
    world: {
      type: 'geojson',
      data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
    }
  },
  version: 8,
  layers: [{
    'id': 'countries',
    'type': 'fill',
    'source': 'world',
    'layout': {},
    'paint': {
      'fill-color': '#6F788A'
    }
  }]
};

  constructor(private route: ActivatedRoute, private destinoApiCliente: DestinosApiClient) {

  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinoApiCliente.getById(id);

  }
}