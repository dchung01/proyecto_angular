import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Destinoviaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, VoteDownAction,VoteUpAction } from '../../models/destino-viajes-states';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: Destinoviaje;
  // tslint:disable-next-line: no-input-rename
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<Destinoviaje>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }


  ngOnInit() {
  }
ir() {
  this.clicked.emit(this.destino);
  this.store.dispatch(new ElegidoFavoritoAction(this.destino));
  return false;
 }

 voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
