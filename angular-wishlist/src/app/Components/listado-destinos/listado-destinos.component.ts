import { Component, OnInit,  EventEmitter, Output } from '@angular/core';
import { Destinoviaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store, State } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction } from '../../models/destino-viajes-states';

@Component({
  selector: 'app-listado-destinos',
  templateUrl: './listado-destinos.component.html',
  styleUrls: ['./listado-destinos.component.css'],
  providers:[DestinosApiClient]

})
export class ListadoDestinosComponent implements OnInit {
  destino: Destinoviaje[];
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onItemAdded: EventEmitter<Destinoviaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if (d != null) {
        this.updates.push('Se ha elegido a ' + d.n);
      }

    });
    // tslint:disable-next-line: no-shadowed-variable
    store.select(State => State.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }
  agregado(d: Destinoviaje) {
   this.destinosApiClient.add(d);
   this.onItemAdded.emit(d);
  }

  elegido(e: Destinoviaje) {
      this.destinosApiClient.elegir(e);
  }

  getAll(){

  }

}
