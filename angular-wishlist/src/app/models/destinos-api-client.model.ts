import { Destinoviaje } from './destino-viaje.model';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destino-viajes-states';
import { Injectable, Inject, forwardRef} from '@angular/core';
import {HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';



@Injectable()
export class DestinosApiClient {

  destinos: Destinoviaje[] = [];


  constructor(private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
    )
  {

    this.store
    .select(state => state.destinos)
    .subscribe((Data) => {
      console.log('destinos sub store');
      console.log(Data);
      this.destinos = Data.items;
    });
    this.store
    .subscribe((Data) => {
      console.log('all store');
      console.log(Data);
    });
  }

add(d: Destinoviaje) {
  const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'}); //tecnica de TOKEN por HEADER
  const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.n }, { headers: headers });
  this.http.request(req).subscribe((data: HttpResponse<{}>) => {
    if (data.status === 200) {
      this.store.dispatch(new NuevoDestinoAction(d));
      const myDb = db;
      myDb.destinos.add(d);
      console.log('todos los destinos de la db!');
      myDb.destinos.toArray().then(destinos => console.log(destinos))
    }
  });
}
  elegir(d: Destinoviaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
  getById(id: string): Destinoviaje {
    // tslint:disable-next-line: only-arrow-functions
    return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
  }
    getAll(): Destinoviaje[] {
      return this.destinos;
    }
}
