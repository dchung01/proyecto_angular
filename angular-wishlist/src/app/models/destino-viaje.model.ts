import {v4 as uuid} from 'uuid';

export class Destinoviaje {
    selected: boolean;
    servicios: string[];
    id = uuid();
    public votes = 0;

    constructor(public n: string, public u: string) {
        this.servicios = ['pileta', 'desayuno'];
    }
        // tslint:disable-next-line: ban-types
        iselected(): Boolean {
            return this.selected;
        }
        Setselected(s: boolean) {
            this.selected = s;
        }

voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}
